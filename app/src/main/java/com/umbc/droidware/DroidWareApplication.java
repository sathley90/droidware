package com.umbc.droidware;

import android.app.Application;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;

/**
 * Created by sushantathley on 3/12/16.
 */
public class DroidWareApplication extends Application {

    public static ParseObject deviceObj = null;

    @Override
    public void onCreate() {
        super.onCreate();

        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_key));
//        Parse.initialize(this, "asd", "adsasdasd");
        ParseUser.enableAutomaticUser();
        final String installationId = ParseInstallation.getCurrentInstallation().getInstallationId();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("tbl_device");
        query.whereEqualTo("device_id", installationId);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (scoreList.size() == 0) {
                    final ParseObject device = new ParseObject("tbl_device");
                    device.put("device_id", installationId);

                    device.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            deviceObj = device;
                        }
                    });

                }

                else deviceObj = scoreList.get(0);


            }
        });


    }
}
