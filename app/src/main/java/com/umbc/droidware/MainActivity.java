package com.umbc.droidware;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AlarmManager alarmManager;
    private PendingIntent alarmIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Register the Alarm and this alarm will call the background service and gather data.
        // Pending Intent is used to create the service.

        // Create an explicit Intent that will be passed to the alarmManager to start the service
        // at regular intervals of time.
        Intent intent = new Intent(this, DataCollectorService.class);
        //Create a pending intent that will wrap the explicit intent "intent"
        alarmIntent = PendingIntent.getService(this, 0 , intent, PendingIntent.FLAG_CANCEL_CURRENT);
        // Take the Handle of the system level Service  In this case it is Alarm Service.
        alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        // Alarm manager will use the pending intent ot start the service at the right amount of time.
        //This will fire the alarm in like 5 seconds from when the application is started.
//        long fireAlarmTime =  SystemClock.elapsedRealtime() + 5 * 1000;
        long fireAlarmTime =  SystemClock.elapsedRealtime() ;
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, fireAlarmTime, 60 * 1000, alarmIntent);


    }
}
