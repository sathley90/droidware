package com.umbc.droidware;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.TrafficStats;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseObject;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by sushantathley on 3/12/16.
 */
public class DataCollectorService extends Service {
    private IntentFilter intentfilter;

    public DataCollectorService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Date d = new Date();
        long date = d.getTime();
        String timeStamp = new Timestamp(date).toString().replace(' ', ',');
        float batLevel = batteryUsageData(timeStamp);
        memoryNetworkProcessInfo(timeStamp,batLevel);
        return super.onStartCommand(intent, flags, startId);
    }

    private float batteryUsageData(String timeStamp) {
        // get the intent when the battery status is changed.
        intentfilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatusIntent = getApplicationContext().registerReceiver(null, intentfilter);

        // Current Battery Level
        int batteryLevel = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        // maximum battery level
        int batteryScale = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        // battery temperature
        int batterytemperature = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
        // battery health
        int batteryHealth = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_HEALTH, -1);
        // battery voltage
        int batteryVoltage = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
        // battery Status
        int batteryStatus = batteryStatusIntent.getIntExtra((BatteryManager.EXTRA_STATUS), -1);
        // battery Technology
        int batteryTechnology = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_TECHNOLOGY, -1);
        // battery Icon Status bar
        int batteryIconStatusBar = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL, -1);
        // battery Present
        int batteryPresent = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_PRESENT, -1);
        //battery Plugged
        int batteryPlugged = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        // Battery Percentage remaining.
        float batteryPct = batteryLevel / (float) batteryScale;


        ParseObject battery = new ParseObject("tbl_battery_stats");
        battery.put("device_id", DroidWareApplication.deviceObj);
        battery.put("battery_lvl", batteryLevel);
        battery.put("battery_temp", batterytemperature);
        battery.put("battery_health", batteryHealth);
        battery.put("battery_voltage", batteryVoltage);
        battery.put("battery_status", batteryStatus);
        battery.put("battery_tech", batteryTechnology);
        battery.put("battery_icon", batteryIconStatusBar);
        battery.put("battery_present", batteryPresent);
        battery.put("battery_plugged", batteryPlugged);
        battery.put("battery_percent", batteryPct);
        battery.put("battery_scale", batteryScale);

        try {
            battery.save();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        // Code to Put the data into the SQL DATABASE.
        return batteryPct;
    }

/*
    private void networkUsageData() {
        */
/*final PackageManager pm = getPackageManager();

        ActivityManager activityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        //final List<ActivityManager.RunningTaskInfo> recentTasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
        for (int i = 0; i < appProcesses.size(); i++) {
            Log.d("Executed app", "Application executed : " + appProcesses.get(i).processName + "\t\t ID: " + appProcesses.get(i).pid + "");
            //  String packageName = activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
            //String packageName = appProcesses.get(i)..getPackageName();
            ApplicationInfo app = null;
            try {
                app = pm.getApplicationInfo(appProcesses.get(i).processName, 0);
                if ((app.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) == 1) {
                    //it's a system app, not interested
                } else if ((app.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                    //Discard this one
                    //in this case, it should be a user-installed app
                } else {
                    // tx = TrafficStats.getUidTxBytes(app.uid);
                    //rx = TrafficStats.getUidRxBytes(app.uid);
                    long delta_rx = TrafficStats.getUidRxBytes(app.uid) - rx;

                    long delta_tx = TrafficStats.getUidTxBytes(app.uid) - tx;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }*//*

    }
*/

    private void memoryNetworkProcessInfo(String timeStamp, float batLevel) {
        long totalMemoryUsage = 0;
        long totalDataUsage = 0;
        Context context = this.getApplicationContext();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        // Store all the running process in a list with the object type RunningAppProcessInfo. UID, ProcessName and PID are present in the info section.
        List<ActivityManager.RunningServiceInfo> runningAppProcessList = activityManager.getRunningServices(Integer.MAX_VALUE);
        // Log.e("DEBUG", "Running processes:");
        totalMemoryUsage = memoryDataUsage(timeStamp, runningAppProcessList, activityManager);
        totalDataUsage = networkDataUsage(timeStamp, runningAppProcessList,activityManager);
        ParseObject totalUsage = new ParseObject("tbl_total_usage");
        totalUsage.put("device_id", DroidWareApplication.deviceObj);
        totalUsage.put("uid", totalMemoryUsage);
        totalUsage.put("total_data_usage", totalDataUsage);
        totalUsage.put("total_battery_status", batLevel);
        totalUsage.put("total_mem_usage", totalMemoryUsage);

        try {
            totalUsage.save();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // data to be sent here. totalMemoryUsage, totaldatausage, batLevel.
        // In this we have to retreive the phone id which will be the primary key.
    }

    private long networkDataUsage(String timeStamp, List<ActivityManager.RunningServiceInfo> runningAppProcessList, ActivityManager activityManager){
        long totalNetworkUsage = 0; // return total bytes (transmitted and received) by all the process.
        for(ActivityManager.RunningServiceInfo runningAppProcessInfo : runningAppProcessList){
            if(runningAppProcessInfo.uid > 1026) {
                String ProcessName = runningAppProcessInfo.process;
                long receievedBytes = TrafficStats.getUidRxBytes(runningAppProcessInfo.uid); // total bytes received by the UID
                long trasmittedBytes = TrafficStats.getUidTxBytes(runningAppProcessInfo.uid);// total bytes Trasmitted by the UID
                totalNetworkUsage += receievedBytes + trasmittedBytes; // total data traffic.
                ParseObject network = new ParseObject("tbl_network");
                network.put("device_id", DroidWareApplication.deviceObj);
                network.put("uid", runningAppProcessInfo.uid);
                network.put("bytes_received", receievedBytes);
                network.put("bytes_transmitted", trasmittedBytes);
                network.put("bytes_total", totalNetworkUsage);
                network.put("process_name", runningAppProcessInfo.process);

                try {
                    network.save();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return totalNetworkUsage;
    }

    private long memoryDataUsage(String timeStamp, List<ActivityManager.RunningServiceInfo> runningAppProcessList, ActivityManager activityManager) {
        // return the total memory used by all the processes. FInd what type is the memory.
        // Filter of the system applications needs to be done. Do you really want to filter it. I dont think it is a good idea.
        // Let me try it without filtering the applications initially.
        // return the total PSS (shared + private) memory used by all the processes.
        long totalMemoryKBytes = 0;
        for (ActivityManager.RunningServiceInfo runningAppProcessInfo : runningAppProcessList) {
            {
/*
                Log.e("DEBUG", "  process name: "+ runningAppProcessInfo.processName);
                Log.e("DEBUG", "  pid: "+ runningAppProcessInfo.pid);
*/
                int[] pid = new int[1];
                pid[0] = runningAppProcessInfo.pid;
                //getProcessMemoryInfo  returns an array of memory information, one for each requested pid.since we can pass and take an array so we are using this for each process.
                android.os.Debug.MemoryInfo[] memoryInfoArray = activityManager.getProcessMemoryInfo(pid);
/*
                Log.e("memory", "     dalvik private: " + memoryInfoArray[0].dalvikPrivateDirty);
                Log.e("memory", "     dalvik shared: " + memoryInfoArray[0].dalvikSharedDirty);
                Log.e("memory", "     dalvik pss: " + memoryInfoArray[0].dalvikPss);
                Log.e("memory", "     native private: " + memoryInfoArray[0].nativePrivateDirty);
                Log.e("memory", "     native shared: " + memoryInfoArray[0].nativeSharedDirty);
                Log.e("memory", "     native pss: " + memoryInfoArray[0].nativePss);
                Log.e("memory", "     other private: " + memoryInfoArray[0].otherPrivateDirty);
                Log.e("memory", "     other shared: " + memoryInfoArray[0].otherSharedDirty);
                Log.e("memory", "     other pss: " + memoryInfoArray[0].otherPss);
*/
                Log.e("ProcessName", "     Process Name " + runningAppProcessInfo.process);
                //PID is transient and keeps on changing.
                Log.e("PID", "      Process ID: " + runningAppProcessInfo.pid);
                // User Id remains same until the app is installed again.
                Log.e("UID", "      User ID: " + runningAppProcessInfo.uid);
                Log.e("memory", "     Total private dirty memory (KB): " + memoryInfoArray[0].getTotalPrivateDirty());
                Log.e("memory", "     Total shared (KB): " + memoryInfoArray[0].getTotalSharedDirty());
                Log.e("memory", "     Total pss: " + memoryInfoArray[0].getTotalPss());
                totalMemoryKBytes += memoryInfoArray[0].getTotalPss();

                ParseObject memory = new ParseObject("tbl_mem_usage");
                memory.put("device_id", DroidWareApplication.deviceObj);
                memory.put("pid", runningAppProcessInfo.pid);
                memory.put("process_name", runningAppProcessInfo.process);
                memory.put("uid", runningAppProcessInfo.uid);
                memory.put("mem_private", memoryInfoArray[0].getTotalPrivateDirty());
                memory.put("mem_shared", memoryInfoArray[0].getTotalSharedDirty());
                memory.put("mem_total_pss", memoryInfoArray[0].getTotalPss());

                try {
                    memory.save();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                // addition of timestamp should be there.//
            }

        }
        return totalMemoryKBytes;
    }
}



